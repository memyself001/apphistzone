var Observable = require("FuseJS/Observable");

var newsList = Observable();

var isLoading = Observable(false);




// Controles de Sidebar
var sidebarOpen = Observable(false);
function setSidebarOpen() {
  sidebarOpen.value = true;
};
function setSidebarClosed() {
  sidebarOpen.value = false;
};





// Objeto Newsfeed
function Newsfeed(backgroundImg_thumbnail, newId, title, subtitle){
     this.backgroundImg_thumbnail = backgroundImg_thumbnail;
     this.newId = newId;
     this.title = title;
     this.subtitle = subtitle;
}





// Llamaa a servidor
function getNewsfeed(e){

     newsList = Observable();

     var myHeaders = new Headers();
     myHeaders.append("content-type", "application/json");
     myHeaders.append("x-apikey", "e766ce77412807ec15128c6fc5c090e9bdabb");
     myHeaders.append("cache-control", "no-cache");

     var myInit = {
          method: 'GET',
          headers: myHeaders
     };

     var url = 'https://histzone-af60.restdb.io/rest/newsfeed?q=%7B%7D&h=%7B%22%24orderby%22%3A%7B%22newId%22%3A-1%7D%7D';


     var myRequest = new Request(url, myInit);

     fetch(myRequest)
          .then(function(result) {
               if (result.status !== 200) {
                    debug_log("Oh dem... Status code: " + result.status);
                    show_error.value = true;
                    isLoading.value = false;
                    return;
               }

               return result.json();
          }).then(function(data) {
               debug_log("Success loading " + url);

               // Llena la lista de Newsfeed
               for(var i=0; i<data.length; i++){
                 newsList.add(new Newsfeed(data[i].backgroundImg_thumbnail, data[i].newId, data[i].title, data[i].subtitle));
               }

               isLoading.value = false;


          }).catch(function(error) {
               debug_log("Oh dem... Fetch error " + error);
               show_error.value = true;

               isLoading.value = false;

          });
}

getNewsfeed();






// Función de Pull To Reload
function reloadHandler(){
  isLoading.value = true;
  getNewsfeed();
}




// Go to a detailed new
function openNew(e){
    router.goto("newdetail");
}






module.exports = {
  newsList: newsList,
  sidebarOpen: sidebarOpen,
  setSidebarOpen: setSidebarOpen,
  setSidebarClosed: setSidebarClosed,
  isLoading: isLoading,
  reloadHandler: reloadHandler,
  openNew: openNew
}
