[Uno.Compiler.UxGenerated]
public partial class NewsfeedPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly NewsfeedPage __parent;
        [Uno.WeakReference] internal readonly NewsfeedPage __parentInstance;
        public Template(NewsfeedPage parent, NewsfeedPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<Fuse.Elements.Visibility> subtitleText_Visibility_inst;
        global::Uno.UX.Property<string> subtitleText_Value_inst;
        global::Uno.UX.Property<float> NewImage_Opacity_inst;
        global::Uno.UX.Property<Fuse.Elements.Visibility> loading_Visibility_inst;
        global::Uno.UX.Property<float> loading_Opacity_inst;
        global::Uno.UX.Property<string> NewImage_Url_inst;
        internal global::Fuse.Controls.Text subtitleText;
        internal global::SimpleLoadingIndicator loading;
        internal global::Fuse.Controls.Image NewImage;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("title");
            subtitleText = new global::Fuse.Controls.Text();
            subtitleText_Visibility_inst = new AppHistZone_FuseElementsElement_Visibility_Property(subtitleText, __selector1);
            subtitleText_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(subtitleText, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("subtitle");
            NewImage = new global::Fuse.Controls.Image();
            NewImage_Opacity_inst = new AppHistZone_FuseElementsElement_Opacity_Property(NewImage, __selector2);
            loading = new global::SimpleLoadingIndicator();
            loading_Visibility_inst = new AppHistZone_FuseElementsElement_Visibility_Property(loading, __selector1);
            loading_Opacity_inst = new AppHistZone_FuseElementsElement_Opacity_Property(loading, __selector2);
            NewImage_Url_inst = new AppHistZone_FuseControlsImage_Url_Property(NewImage, __selector3);
            var temp3 = new global::Fuse.Reactive.Data("backgroundImg_thumbnail");
            var temp4 = new global::Fuse.Controls.StackPanel();
            var temp5 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
            var temp6 = new global::Fuse.Triggers.WhileContainsText();
            var temp7 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(subtitleText_Visibility_inst);
            var temp8 = new global::Fuse.Reactive.DataBinding(subtitleText_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
            var temp9 = new global::Fuse.Controls.Text();
            var temp10 = new global::Fuse.Controls.Rectangle();
            var temp11 = new global::Fuse.Drawing.LinearGradient();
            var temp12 = new global::Fuse.Drawing.GradientStop();
            var temp13 = new global::Fuse.Drawing.GradientStop();
            var temp14 = new global::Fuse.Triggers.WhileBusy();
            var temp15 = new global::Fuse.Animations.Change<float>(NewImage_Opacity_inst);
            var temp16 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(loading_Visibility_inst);
            var temp17 = new global::Fuse.Animations.Change<float>(loading_Opacity_inst);
            var temp18 = new global::Fuse.Reactive.DataBinding(NewImage_Url_inst, temp3, Fuse.Reactive.BindingMode.Default);
            __self.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            __self.Height = new Uno.UX.Size(145f, Uno.UX.Unit.Unspecified);
            __self.Margin = float4(0f, 0f, 0f, 3f);
            temp4.Alignment = Fuse.Elements.Alignment.Bottom;
            temp4.Children.Add(temp);
            temp4.Children.Add(subtitleText);
            temp4.Children.Add(temp9);
            temp.FontSize = 19f;
            temp.Color = Fuse.Drawing.Colors.White;
            temp.Margin = float4(10f, 0f, 10f, 2f);
            temp.Font = global::MainView.Bold;
            temp.Bindings.Add(temp5);
            subtitleText.FontSize = 15f;
            subtitleText.Color = Fuse.Drawing.Colors.White;
            subtitleText.Visibility = Fuse.Elements.Visibility.Collapsed;
            subtitleText.Margin = float4(10f, 0f, 10f, 6f);
            subtitleText.Name = __selector4;
            subtitleText.Font = global::MainView.Regular;
            subtitleText.Children.Add(temp6);
            subtitleText.Bindings.Add(temp8);
            temp6.Animators.Add(temp7);
            temp7.Value = Fuse.Elements.Visibility.Visible;
            temp9.Value = "19 julio 2018";
            temp9.FontSize = 10f;
            temp9.Color = Fuse.Drawing.Colors.White;
            temp9.Margin = float4(10f, 0f, 10f, 4f);
            temp9.Font = global::MainView.Light;
            loading.Visibility = Fuse.Elements.Visibility.Collapsed;
            loading.Opacity = 0f;
            loading.Name = __selector5;
            temp10.Fills.Add(temp11);
            temp11.StartPoint = float2(0f, 0f);
            temp11.EndPoint = float2(0f, 1f);
            temp11.Opacity = 0.8f;
            temp11.Stops.Add(temp12);
            temp11.Stops.Add(temp13);
            temp12.Offset = 0f;
            temp12.Color = Fuse.Drawing.Colors.Transparent;
            temp13.Offset = 0.6f;
            temp13.Color = float4(0f, 0f, 0f, 1f);
            NewImage.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
            NewImage.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            NewImage.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            NewImage.Name = __selector6;
            NewImage.Children.Add(temp14);
            NewImage.Bindings.Add(temp18);
            temp14.Animators.Add(temp15);
            temp14.Animators.Add(temp16);
            temp14.Animators.Add(temp17);
            temp15.Value = 0f;
            temp15.Duration = 1;
            temp16.Value = Fuse.Elements.Visibility.Visible;
            temp17.Value = 1f;
            temp17.Duration = 0.3;
            __self.Children.Add(temp4);
            __self.Children.Add(loading);
            __self.Children.Add(temp10);
            __self.Children.Add(NewImage);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "Visibility";
        static global::Uno.UX.Selector __selector2 = "Opacity";
        static global::Uno.UX.Selector __selector3 = "Url";
        static global::Uno.UX.Selector __selector4 = "subtitleText";
        static global::Uno.UX.Selector __selector5 = "loading";
        static global::Uno.UX.Selector __selector6 = "NewImage";
    }
    global::Uno.UX.Property<float> mainAppTranslation_X_inst;
    global::Uno.UX.Property<float> topMenuTranslation_Y_inst;
    global::Uno.UX.Property<float> bottomMenuTranslation_Y_inst;
    global::Uno.UX.Property<float> middleRectangle_Opacity_inst;
    global::Uno.UX.Property<float> topMenuRotation_Degrees_inst;
    global::Uno.UX.Property<float> bottomMenuRotation_Degrees_inst;
    global::Uno.UX.Property<Uno.UX.Size> topRectangle_Width_inst;
    global::Uno.UX.Property<Uno.UX.Size> bottomRectangle_Width_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<object> theEach_Items_inst;
    internal global::Fuse.Controls.EdgeNavigator EdgeNavigator;
    internal global::Sidebar menu;
    internal global::Fuse.Reactive.EventBinding temp_eb0;
    internal global::Fuse.Reactive.EventBinding temp_eb1;
    internal global::Fuse.Controls.DockPanel content;
    internal global::Fuse.Translation mainAppTranslation;
    internal global::Fuse.Controls.Rectangle topRectangle;
    internal global::Fuse.Translation topMenuTranslation;
    internal global::Fuse.Rotation topMenuRotation;
    internal global::Fuse.Controls.Rectangle middleRectangle;
    internal global::Fuse.Controls.Rectangle bottomRectangle;
    internal global::Fuse.Translation bottomMenuTranslation;
    internal global::Fuse.Rotation bottomMenuRotation;
    internal global::Fuse.Controls.ScrollView scrollView;
    internal global::Fuse.Reactive.Each theEach;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "EdgeNavigator",
        "menu",
        "temp_eb0",
        "temp_eb1",
        "content",
        "mainAppTranslation",
        "topRectangle",
        "topMenuTranslation",
        "topMenuRotation",
        "middleRectangle",
        "bottomRectangle",
        "bottomMenuTranslation",
        "bottomMenuRotation",
        "scrollView",
        "theEach"
    };
    static NewsfeedPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public NewsfeedPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        mainAppTranslation = new global::Fuse.Translation();
        mainAppTranslation_X_inst = new AppHistZone_FuseTranslation_X_Property(mainAppTranslation, __selector0);
        topMenuTranslation = new global::Fuse.Translation();
        topMenuTranslation_Y_inst = new AppHistZone_FuseTranslation_Y_Property(topMenuTranslation, __selector1);
        bottomMenuTranslation = new global::Fuse.Translation();
        bottomMenuTranslation_Y_inst = new AppHistZone_FuseTranslation_Y_Property(bottomMenuTranslation, __selector1);
        middleRectangle = new global::Fuse.Controls.Rectangle();
        middleRectangle_Opacity_inst = new AppHistZone_FuseElementsElement_Opacity_Property(middleRectangle, __selector2);
        topMenuRotation = new global::Fuse.Rotation();
        topMenuRotation_Degrees_inst = new AppHistZone_FuseRotation_Degrees_Property(topMenuRotation, __selector3);
        bottomMenuRotation = new global::Fuse.Rotation();
        bottomMenuRotation_Degrees_inst = new AppHistZone_FuseRotation_Degrees_Property(bottomMenuRotation, __selector3);
        topRectangle = new global::Fuse.Controls.Rectangle();
        topRectangle_Width_inst = new AppHistZone_FuseElementsElement_Width_Property(topRectangle, __selector4);
        bottomRectangle = new global::Fuse.Controls.Rectangle();
        bottomRectangle_Width_inst = new AppHistZone_FuseElementsElement_Width_Property(bottomRectangle, __selector4);
        var temp2 = new global::Fuse.Reactive.Data("setSidebarOpen");
        var temp3 = new global::Fuse.Reactive.Data("setSidebarClosed");
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new AppHistZone_FuseTriggersWhileBool_Value_Property(temp, __selector5);
        var temp4 = new global::Fuse.Reactive.Data("sidebarOpen");
        var temp1 = new global::Fuse.Triggers.WhileFalse();
        temp1_Value_inst = new AppHistZone_FuseTriggersWhileBool_Value_Property(temp1, __selector5);
        var temp5 = new global::Fuse.Reactive.Data("sidebarOpen");
        theEach = new global::Fuse.Reactive.Each();
        theEach_Items_inst = new AppHistZone_FuseReactiveEach_Items_Property(theEach, __selector6);
        var temp6 = new global::Fuse.Reactive.Data("newsList");
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        EdgeNavigator = new global::Fuse.Controls.EdgeNavigator();
        menu = new global::Sidebar();
        var temp8 = new global::Fuse.Navigation.ActivatingAnimation();
        var temp9 = new global::Fuse.Animations.Change<float>(mainAppTranslation_X_inst);
        var temp10 = new global::Fuse.Animations.Change<float>(topMenuTranslation_Y_inst);
        var temp11 = new global::Fuse.Animations.Change<float>(bottomMenuTranslation_Y_inst);
        var temp12 = new global::Fuse.Animations.Change<float>(middleRectangle_Opacity_inst);
        var temp13 = new global::Fuse.Animations.Change<float>(topMenuRotation_Degrees_inst);
        var temp14 = new global::Fuse.Animations.Change<float>(bottomMenuRotation_Degrees_inst);
        var temp15 = new global::Fuse.Animations.Change<Uno.UX.Size>(topRectangle_Width_inst);
        var temp16 = new global::Fuse.Animations.Change<Uno.UX.Size>(bottomRectangle_Width_inst);
        var temp17 = new global::Fuse.Navigation.WhileActive();
        var temp18 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb0 = new global::Fuse.Reactive.EventBinding(temp2);
        var temp19 = new global::Fuse.Navigation.WhileInactive();
        var temp20 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb1 = new global::Fuse.Reactive.EventBinding(temp3);
        content = new global::Fuse.Controls.DockPanel();
        var temp21 = new global::Fuse.Controls.DockPanel();
        var temp22 = new global::Fuse.Controls.Image();
        var temp23 = new global::Fuse.Controls.Image();
        var temp24 = new global::Fuse.Controls.Panel();
        var temp25 = new global::Fuse.Gestures.Clicked();
        var temp26 = new global::Fuse.Navigation.NavigateTo();
        var temp27 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp28 = new global::Fuse.Gestures.Clicked();
        var temp29 = new global::Fuse.Navigation.NavigateTo();
        var temp30 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        scrollView = new global::Fuse.Controls.ScrollView();
        var temp31 = new global::Fuse.Controls.StackPanel();
        var temp32 = new Template(this, this);
        var temp33 = new global::Fuse.Reactive.DataBinding(theEach_Items_inst, temp6, Fuse.Reactive.BindingMode.Default);
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        global::Fuse.Controls.Navigator.SetReuse(this, Fuse.Controls.ReuseType.Any);
        temp7.LineNumber = 4;
        temp7.FileName = "Pages/NewsfeedPage.ux";
        temp7.File = new global::Uno.UX.BundleFileSource(import("../../Pages/NewsfeedPage.js"));
        EdgeNavigator.Name = __selector7;
        EdgeNavigator.Children.Add(menu);
        EdgeNavigator.Children.Add(content);
        menu.Width = new Uno.UX.Size(280f, Uno.UX.Unit.Unspecified);
        menu.Name = __selector8;
        global::Fuse.Navigation.EdgeNavigation.SetEdge(menu, Fuse.Navigation.NavigationEdge.Left);
        menu.Children.Add(temp8);
        menu.Children.Add(temp17);
        menu.Children.Add(temp19);
        temp8.Animators.Add(temp9);
        temp8.Animators.Add(temp10);
        temp8.Animators.Add(temp11);
        temp8.Animators.Add(temp12);
        temp8.Animators.Add(temp13);
        temp8.Animators.Add(temp14);
        temp8.Animators.Add(temp15);
        temp8.Animators.Add(temp16);
        temp9.Value = 280f;
        temp10.Value = 0f;
        temp11.Value = 0f;
        temp12.Value = 0f;
        temp12.Easing = Fuse.Animations.Easing.CircularOut;
        temp13.Value = 45f;
        temp13.Easing = Fuse.Animations.Easing.ExponentialIn;
        temp14.Value = -45f;
        temp14.Easing = Fuse.Animations.Easing.ExponentialIn;
        temp15.Value = new Uno.UX.Size(28f, Uno.UX.Unit.Unspecified);
        temp16.Value = new Uno.UX.Size(28f, Uno.UX.Unit.Unspecified);
        temp17.Actions.Add(temp18);
        temp17.Bindings.Add(temp_eb0);
        temp18.Handler += temp_eb0.OnEvent;
        temp19.Actions.Add(temp20);
        temp19.Bindings.Add(temp_eb1);
        temp20.Handler += temp_eb1.OnEvent;
        content.Color = float4(0.654902f, 0.6431373f, 0.6352941f, 1f);
        content.Name = __selector9;
        content.Children.Add(mainAppTranslation);
        content.Children.Add(temp21);
        content.Children.Add(scrollView);
        mainAppTranslation.Name = __selector10;
        temp21.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp21.Height = new Uno.UX.Size(15f, Uno.UX.Unit.Percent);
        global::Fuse.Controls.DockPanel.SetDock(temp21, Fuse.Layouts.Dock.Top);
        temp21.Children.Add(temp22);
        temp21.Children.Add(temp23);
        temp21.Children.Add(temp24);
        temp22.Height = new Uno.UX.Size(31f, Uno.UX.Unit.Percent);
        temp22.Margin = float4(15f, 15f, 15f, 15f);
        temp22.Layer = Fuse.Layer.Background;
        temp22.File = new global::Uno.UX.BundleFileSource(import("../../Assets/NewsfeedTitle.png"));
        temp23.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp23.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp23.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp23.Opacity = 1f;
        temp23.Layer = Fuse.Layer.Background;
        temp23.File = new global::Uno.UX.BundleFileSource(import("../../Assets/NewsfeedBackground.png"));
        temp24.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        temp24.Width = new Uno.UX.Size(32f, Uno.UX.Unit.Unspecified);
        temp24.Height = new Uno.UX.Size(32f, Uno.UX.Unit.Unspecified);
        temp24.Margin = float4(15f, 15f, 15f, 15f);
        global::Fuse.Controls.DockPanel.SetDock(temp24, Fuse.Layouts.Dock.Left);
        temp24.Children.Add(temp);
        temp24.Children.Add(temp1);
        temp24.Children.Add(topRectangle);
        temp24.Children.Add(middleRectangle);
        temp24.Children.Add(bottomRectangle);
        temp.Nodes.Add(temp25);
        temp.Bindings.Add(temp27);
        temp25.Actions.Add(temp26);
        temp26.Target = content;
        temp1.Nodes.Add(temp28);
        temp1.Bindings.Add(temp30);
        temp28.Actions.Add(temp29);
        temp29.Target = menu;
        topRectangle.Color = float4(0.9764706f, 0.2392157f, 0.05098039f, 1f);
        topRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        topRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        topRectangle.Name = __selector11;
        topRectangle.Children.Add(topMenuTranslation);
        topRectangle.Children.Add(topMenuRotation);
        topMenuTranslation.Y = -9f;
        topMenuTranslation.Name = __selector12;
        topMenuRotation.Name = __selector13;
        middleRectangle.Color = float4(0.9764706f, 0.2392157f, 0.05098039f, 1f);
        middleRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        middleRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        middleRectangle.Name = __selector14;
        bottomRectangle.Color = float4(0.9764706f, 0.2392157f, 0.05098039f, 1f);
        bottomRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        bottomRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        bottomRectangle.Name = __selector15;
        bottomRectangle.Children.Add(bottomMenuTranslation);
        bottomRectangle.Children.Add(bottomMenuRotation);
        bottomMenuTranslation.Y = 9f;
        bottomMenuTranslation.Name = __selector16;
        bottomMenuRotation.Name = __selector17;
        scrollView.Name = __selector18;
        global::Fuse.Controls.DockPanel.SetDock(scrollView, Fuse.Layouts.Dock.Fill);
        scrollView.Children.Add(temp31);
        temp31.Children.Add(theEach);
        theEach.Name = __selector19;
        theEach.Templates.Add(temp32);
        theEach.Bindings.Add(temp33);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(EdgeNavigator);
        __g_nametable.Objects.Add(menu);
        __g_nametable.Objects.Add(temp_eb0);
        __g_nametable.Objects.Add(temp_eb1);
        __g_nametable.Objects.Add(content);
        __g_nametable.Objects.Add(mainAppTranslation);
        __g_nametable.Objects.Add(topRectangle);
        __g_nametable.Objects.Add(topMenuTranslation);
        __g_nametable.Objects.Add(topMenuRotation);
        __g_nametable.Objects.Add(middleRectangle);
        __g_nametable.Objects.Add(bottomRectangle);
        __g_nametable.Objects.Add(bottomMenuTranslation);
        __g_nametable.Objects.Add(bottomMenuRotation);
        __g_nametable.Objects.Add(scrollView);
        __g_nametable.Objects.Add(theEach);
        this.Children.Add(temp7);
        this.Children.Add(EdgeNavigator);
    }
    static global::Uno.UX.Selector __selector0 = "X";
    static global::Uno.UX.Selector __selector1 = "Y";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "Degrees";
    static global::Uno.UX.Selector __selector4 = "Width";
    static global::Uno.UX.Selector __selector5 = "Value";
    static global::Uno.UX.Selector __selector6 = "Items";
    static global::Uno.UX.Selector __selector7 = "EdgeNavigator";
    static global::Uno.UX.Selector __selector8 = "menu";
    static global::Uno.UX.Selector __selector9 = "content";
    static global::Uno.UX.Selector __selector10 = "mainAppTranslation";
    static global::Uno.UX.Selector __selector11 = "topRectangle";
    static global::Uno.UX.Selector __selector12 = "topMenuTranslation";
    static global::Uno.UX.Selector __selector13 = "topMenuRotation";
    static global::Uno.UX.Selector __selector14 = "middleRectangle";
    static global::Uno.UX.Selector __selector15 = "bottomRectangle";
    static global::Uno.UX.Selector __selector16 = "bottomMenuTranslation";
    static global::Uno.UX.Selector __selector17 = "bottomMenuRotation";
    static global::Uno.UX.Selector __selector18 = "scrollView";
    static global::Uno.UX.Selector __selector19 = "theEach";
}
