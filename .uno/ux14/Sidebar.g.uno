[Uno.Compiler.UxGenerated]
public partial class Sidebar: Fuse.Controls.Panel
{
    internal global::Fuse.Controls.StackPanel userData;
    internal global::Fuse.Controls.StackPanel NewsPanel;
    internal global::Fuse.Controls.StackPanel BookingPanel;
    internal global::Fuse.Controls.StackPanel AboutPanel;
    internal global::Fuse.Controls.StackPanel ContactPanel;
    static Sidebar()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Sidebar()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Navigation.WhileInactive();
        var temp1 = new global::Fuse.Animations.Move();
        var temp2 = new global::Fuse.Animations.Move();
        var temp3 = new global::Fuse.Animations.Move();
        var temp4 = new global::Fuse.Animations.Move();
        var temp5 = new global::Fuse.Animations.Move();
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Fuse.Controls.DockPanel();
        userData = new global::Fuse.Controls.StackPanel();
        var temp8 = new global::Fuse.Controls.Circle();
        var temp9 = new global::Fuse.Drawing.ImageFill();
        var temp10 = new global::Fuse.Controls.Text();
        var temp11 = new global::Fuse.Controls.Text();
        var temp12 = new global::Fuse.Controls.DockPanel();
        var temp13 = new global::Fuse.Controls.StackPanel();
        NewsPanel = new global::Fuse.Controls.StackPanel();
        var temp14 = new global::Fuse.Controls.Image();
        var temp15 = new global::Fuse.Controls.Text();
        BookingPanel = new global::Fuse.Controls.StackPanel();
        var temp16 = new global::Fuse.Controls.Image();
        var temp17 = new global::Fuse.Controls.Text();
        AboutPanel = new global::Fuse.Controls.StackPanel();
        var temp18 = new global::Fuse.Controls.Image();
        var temp19 = new global::Fuse.Controls.Text();
        ContactPanel = new global::Fuse.Controls.StackPanel();
        var temp20 = new global::Fuse.Controls.Image();
        var temp21 = new global::Fuse.Controls.Text();
        var temp22 = new global::Fuse.Controls.Text();
        var temp23 = new global::Fuse.Controls.DockPanel();
        var temp24 = new global::Fuse.Controls.StackPanel();
        var temp25 = new global::Fuse.Controls.StackPanel();
        var temp26 = new global::Fuse.Controls.Image();
        var temp27 = new global::Fuse.Controls.Text();
        var temp28 = new global::Fuse.Controls.Text();
        var temp29 = new global::Fuse.Controls.Image();
        temp.Threshold = 0.4f;
        temp.Animators.Add(temp1);
        temp.Animators.Add(temp2);
        temp.Animators.Add(temp3);
        temp.Animators.Add(temp4);
        temp.Animators.Add(temp5);
        temp1.X = -180f;
        temp1.Duration = 0.2;
        temp1.Delay = 0.3;
        temp1.Target = userData;
        temp1.Easing = Fuse.Animations.Easing.CircularIn;
        temp2.X = -180f;
        temp2.Duration = 0.2;
        temp2.Delay = 0.2;
        temp2.Target = NewsPanel;
        temp2.Easing = Fuse.Animations.Easing.CircularIn;
        temp3.X = -180f;
        temp3.Duration = 0.2;
        temp3.Delay = 0.15;
        temp3.Target = BookingPanel;
        temp3.Easing = Fuse.Animations.Easing.CircularIn;
        temp4.X = -180f;
        temp4.Duration = 0.2;
        temp4.Delay = 0.1;
        temp4.Target = AboutPanel;
        temp4.Easing = Fuse.Animations.Easing.CircularIn;
        temp5.X = -180f;
        temp5.Duration = 0.2;
        temp5.Delay = 0.05;
        temp5.Target = ContactPanel;
        temp5.Easing = Fuse.Animations.Easing.CircularIn;
        temp6.Children.Add(temp7);
        temp7.Color = float4(0.1098039f, 0.1411765f, 0.1921569f, 1f);
        temp7.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp7.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Left);
        temp7.Children.Add(userData);
        temp7.Children.Add(temp12);
        temp7.Children.Add(temp23);
        userData.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        userData.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        userData.Name = __selector0;
        global::Fuse.Controls.DockPanel.SetDock(userData, Fuse.Layouts.Dock.Top);
        userData.Children.Add(temp8);
        userData.Children.Add(temp10);
        userData.Children.Add(temp11);
        temp8.Color = Fuse.Drawing.Colors.White;
        temp8.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        temp8.Height = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        temp8.Margin = float4(0f, 40f, 0f, 0f);
        temp8.Fills.Add(temp9);
        temp9.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp9.File = new global::Uno.UX.BundleFileSource(import("../../Assets/user01.jpg"));
        temp10.Value = "Eloisa de la Cruz";
        temp10.FontSize = 17f;
        temp10.Color = Fuse.Drawing.Colors.White;
        temp10.Alignment = Fuse.Elements.Alignment.Center;
        temp10.Margin = float4(0f, 10f, 0f, 0f);
        temp10.Font = global::MainView.Light;
        temp11.Value = "Administrador";
        temp11.FontSize = 14f;
        temp11.Color = Fuse.Drawing.Colors.White;
        temp11.Alignment = Fuse.Elements.Alignment.Center;
        temp11.Margin = float4(0f, 1f, 0f, 0f);
        temp11.Font = global::MainView.Bold;
        temp12.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        global::Fuse.Controls.DockPanel.SetDock(temp12, Fuse.Layouts.Dock.Fill);
        temp12.Children.Add(temp13);
        temp12.Children.Add(temp22);
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Top);
        temp13.Children.Add(NewsPanel);
        temp13.Children.Add(BookingPanel);
        temp13.Children.Add(AboutPanel);
        temp13.Children.Add(ContactPanel);
        NewsPanel.Orientation = Fuse.Layouts.Orientation.Horizontal;
        NewsPanel.Margin = float4(0f, 25f, 0f, 0f);
        NewsPanel.Name = __selector1;
        NewsPanel.Children.Add(temp14);
        NewsPanel.Children.Add(temp15);
        temp14.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp14.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp14.Margin = float4(25f, 0f, 15f, 0f);
        temp14.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconNews.png"));
        temp15.Value = "Actualidad";
        temp15.FontSize = 17f;
        temp15.Color = Fuse.Drawing.Colors.White;
        temp15.Alignment = Fuse.Elements.Alignment.Center;
        temp15.Font = global::MainView.Light;
        BookingPanel.Orientation = Fuse.Layouts.Orientation.Horizontal;
        BookingPanel.Margin = float4(0f, 33f, 0f, 0f);
        BookingPanel.Name = __selector2;
        BookingPanel.Children.Add(temp16);
        BookingPanel.Children.Add(temp17);
        temp16.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp16.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp16.Margin = float4(25f, 0f, 15f, 0f);
        temp16.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconBooking.png"));
        temp17.Value = "Reservas";
        temp17.FontSize = 17f;
        temp17.Color = Fuse.Drawing.Colors.White;
        temp17.Alignment = Fuse.Elements.Alignment.Center;
        temp17.Font = global::MainView.Light;
        AboutPanel.Orientation = Fuse.Layouts.Orientation.Horizontal;
        AboutPanel.Margin = float4(0f, 33f, 0f, 0f);
        AboutPanel.Name = __selector3;
        AboutPanel.Children.Add(temp18);
        AboutPanel.Children.Add(temp19);
        temp18.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp18.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp18.Margin = float4(25f, 0f, 15f, 0f);
        temp18.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconAbout.png"));
        temp19.Value = "Información";
        temp19.FontSize = 17f;
        temp19.Color = Fuse.Drawing.Colors.White;
        temp19.Alignment = Fuse.Elements.Alignment.Center;
        temp19.Font = global::MainView.Light;
        ContactPanel.Orientation = Fuse.Layouts.Orientation.Horizontal;
        ContactPanel.Margin = float4(0f, 33f, 0f, 0f);
        ContactPanel.Name = __selector4;
        ContactPanel.Children.Add(temp20);
        ContactPanel.Children.Add(temp21);
        temp20.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp20.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp20.Margin = float4(25f, 0f, 15f, 0f);
        temp20.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconContact.png"));
        temp21.Value = "Contáctanos";
        temp21.FontSize = 17f;
        temp21.Color = Fuse.Drawing.Colors.White;
        temp21.Alignment = Fuse.Elements.Alignment.Center;
        temp21.Font = global::MainView.Light;
        temp22.Value = "Cerrar sesión";
        temp22.FontSize = 12f;
        temp22.Color = float4(0.9764706f, 0.2392157f, 0.05098039f, 1f);
        temp22.Alignment = Fuse.Elements.Alignment.Center;
        temp22.Margin = float4(15f, 15f, 15f, 15f);
        global::Fuse.Controls.DockPanel.SetDock(temp22, Fuse.Layouts.Dock.Bottom);
        temp22.Font = global::MainView.Light;
        temp23.Color = float4(0.1921569f, 0.227451f, 0.2705882f, 1f);
        temp23.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp23.Height = new Uno.UX.Size(9.5f, Uno.UX.Unit.Percent);
        global::Fuse.Controls.DockPanel.SetDock(temp23, Fuse.Layouts.Dock.Bottom);
        temp23.Children.Add(temp24);
        temp23.Children.Add(temp29);
        temp24.Orientation = Fuse.Layouts.Orientation.Horizontal;
        global::Fuse.Controls.DockPanel.SetDock(temp24, Fuse.Layouts.Dock.Fill);
        temp24.Children.Add(temp25);
        temp25.Orientation = Fuse.Layouts.Orientation.Horizontal;
        temp25.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp25.Alignment = Fuse.Elements.Alignment.Center;
        temp25.Children.Add(temp26);
        temp25.Children.Add(temp27);
        temp25.Children.Add(temp28);
        temp26.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp26.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp26.Margin = float4(10f, 0f, 15f, 0f);
        temp26.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconLanguage.png"));
        temp27.Value = "Idioma";
        temp27.FontSize = 17f;
        temp27.Color = Fuse.Drawing.Colors.White;
        temp27.Alignment = Fuse.Elements.Alignment.Center;
        temp27.Font = global::MainView.Regular;
        temp28.Value = "Español";
        temp28.FontSize = 14f;
        temp28.Color = Fuse.Drawing.Colors.White;
        temp28.Alignment = Fuse.Elements.Alignment.Center;
        temp28.Margin = float4(28f, 0f, 0f, 0f);
        temp28.Font = global::MainView.Regular;
        temp29.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp29.Width = new Uno.UX.Size(6f, Uno.UX.Unit.Unspecified);
        temp29.Alignment = Fuse.Elements.Alignment.Center;
        temp29.Margin = float4(20f, 20f, 20f, 20f);
        global::Fuse.Controls.DockPanel.SetDock(temp29, Fuse.Layouts.Dock.Right);
        temp29.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconRightArrow.png"));
        this.Children.Add(temp);
        this.Children.Add(temp6);
    }
    static global::Uno.UX.Selector __selector0 = "userData";
    static global::Uno.UX.Selector __selector1 = "NewsPanel";
    static global::Uno.UX.Selector __selector2 = "BookingPanel";
    static global::Uno.UX.Selector __selector3 = "AboutPanel";
    static global::Uno.UX.Selector __selector4 = "ContactPanel";
}
