[Uno.Compiler.UxGenerated]
public partial class SimpleLoadingIndicator: Fuse.Controls.Panel
{
    float4 _field_ThemeColor;
    [global::Uno.UX.UXOriginSetter("SetThemeColor")]
    public float4 ThemeColor
    {
        get { return _field_ThemeColor; }
        set { SetThemeColor(value, null); }
    }
    public void SetThemeColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_ThemeColor)
        {
            _field_ThemeColor = value;
            OnPropertyChanged("ThemeColor", origin);
        }
    }
    internal global::Fuse.Controls.Image rotatingStroke;
    static SimpleLoadingIndicator()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SimpleLoadingIndicator()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        rotatingStroke = new global::Fuse.Controls.Image();
        var temp = new global::Fuse.Triggers.WhileFalse();
        var temp1 = new global::Fuse.Animations.Spin();
        this.ThemeColor = float4(0.9764706f, 0.2392157f, 0.05098039f, 1f);
        rotatingStroke.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        rotatingStroke.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        rotatingStroke.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        rotatingStroke.Name = __selector0;
        rotatingStroke.File = new global::Uno.UX.BundleFileSource(import("../../Assets/Loading1.png"));
        temp.Animators.Add(temp1);
        temp1.Frequency = 1;
        temp1.Target = rotatingStroke;
        this.Children.Add(rotatingStroke);
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "rotatingStroke";
}
