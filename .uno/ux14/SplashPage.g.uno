[Uno.Compiler.UxGenerated]
public partial class SplashPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    internal global::Fuse.Controls.Image SplashLogo;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "SplashLogo"
    };
    static SplashPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SplashPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.JavaScript(__g_nametable);
        SplashLogo = new global::Fuse.Controls.Image();
        var temp1 = new global::Fuse.Navigation.WhileActive();
        var temp2 = new global::Fuse.Animations.Rotate();
        var temp3 = new global::Fuse.Animations.Rotate();
        var temp4 = new global::Fuse.Controls.Rectangle();
        var temp5 = new global::Fuse.Drawing.LinearGradient();
        var temp6 = new global::Fuse.Drawing.GradientStop();
        var temp7 = new global::Fuse.Drawing.GradientStop();
        var temp8 = new global::Fuse.Controls.Image();
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        global::Fuse.Controls.Navigator.SetReuse(this, Fuse.Controls.ReuseType.Any);
        temp.LineNumber = 5;
        temp.FileName = "Pages/SplashPage.ux";
        temp.File = new global::Uno.UX.BundleFileSource(import("../../Pages/SplashPage.js"));
        SplashLogo.StretchMode = Fuse.Elements.StretchMode.Uniform;
        SplashLogo.Width = new Uno.UX.Size(28f, Uno.UX.Unit.Percent);
        SplashLogo.Name = __selector0;
        SplashLogo.File = new global::Uno.UX.BundleFileSource(import("../../Assets/SplashLogo.png"));
        SplashLogo.Children.Add(temp1);
        temp1.Animators.Add(temp2);
        temp1.Animators.Add(temp3);
        temp2.Degrees = 1080f;
        temp2.Duration = 2;
        temp2.Delay = 0.4;
        temp2.Easing = Fuse.Animations.Easing.CircularOut;
        temp3.Degrees = 1080f;
        temp3.Duration = 18;
        temp3.Delay = 2;
        temp4.Opacity = 0.43f;
        temp4.Fills.Add(temp5);
        temp5.StartPoint = float2(0f, 0f);
        temp5.EndPoint = float2(0f, 1f);
        temp5.Stops.Add(temp6);
        temp5.Stops.Add(temp7);
        temp6.Offset = 0f;
        temp6.Color = float4(0.5254902f, 0.3882353f, 0.3294118f, 1f);
        temp7.Offset = 1f;
        temp7.Color = float4(1f, 0.3176471f, 0f, 1f);
        temp8.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Layer = Fuse.Layer.Background;
        temp8.File = new global::Uno.UX.BundleFileSource(import("../../Assets/SplashBackground.png"));
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(SplashLogo);
        this.Children.Add(temp);
        this.Children.Add(SplashLogo);
        this.Children.Add(temp4);
        this.Children.Add(temp8);
    }
    static global::Uno.UX.Selector __selector0 = "SplashLogo";
}
