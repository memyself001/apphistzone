[Uno.Compiler.UxGenerated]
public partial class NewsfeedPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly NewsfeedPage __parent;
        [Uno.WeakReference] internal readonly NewsfeedPage __parentInstance;
        public Template(NewsfeedPage parent, NewsfeedPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<Fuse.Elements.Visibility> subtitleText_Visibility_inst;
        global::Uno.UX.Property<string> subtitleText_Value_inst;
        global::Uno.UX.Property<string> temp1_Url_inst;
        internal global::Fuse.Controls.Text subtitleText;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("title");
            subtitleText = new global::Fuse.Controls.Text();
            subtitleText_Visibility_inst = new AppHistZone_FuseElementsElement_Visibility_Property(subtitleText, __selector1);
            subtitleText_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(subtitleText, __selector0);
            var temp3 = new global::Fuse.Reactive.Data("subtitle");
            var temp1 = new global::Fuse.Controls.Image();
            temp1_Url_inst = new AppHistZone_FuseControlsImage_Url_Property(temp1, __selector2);
            var temp4 = new global::Fuse.Reactive.Data("backgroundImg_thumbnail");
            var temp5 = new global::Fuse.Controls.StackPanel();
            var temp6 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
            var temp7 = new global::Fuse.Triggers.WhileContainsText();
            var temp8 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(subtitleText_Visibility_inst);
            var temp9 = new global::Fuse.Reactive.DataBinding(subtitleText_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
            var temp10 = new global::Fuse.Controls.Text();
            var temp11 = new global::Fuse.Controls.Rectangle();
            var temp12 = new global::Fuse.Drawing.LinearGradient();
            var temp13 = new global::Fuse.Drawing.GradientStop();
            var temp14 = new global::Fuse.Drawing.GradientStop();
            var temp15 = new global::Fuse.Reactive.DataBinding(temp1_Url_inst, temp4, Fuse.Reactive.BindingMode.Default);
            __self.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            __self.Height = new Uno.UX.Size(145f, Uno.UX.Unit.Unspecified);
            __self.Margin = float4(0f, 0f, 0f, 3f);
            __self.SourceLineNumber = 39;
            __self.SourceFileName = "Pages/NewsfeedPage.ux";
            temp5.Alignment = Fuse.Elements.Alignment.Bottom;
            temp5.SourceLineNumber = 40;
            temp5.SourceFileName = "Pages/NewsfeedPage.ux";
            temp5.Children.Add(temp);
            temp5.Children.Add(subtitleText);
            temp5.Children.Add(temp10);
            temp.FontSize = 19f;
            temp.Color = Fuse.Drawing.Colors.White;
            temp.Margin = float4(10f, 0f, 10f, 2f);
            temp.SourceLineNumber = 41;
            temp.SourceFileName = "Pages/NewsfeedPage.ux";
            temp.Bindings.Add(temp6);
            temp2.SourceLineNumber = 41;
            temp2.SourceFileName = "Pages/NewsfeedPage.ux";
            subtitleText.FontSize = 15f;
            subtitleText.Color = Fuse.Drawing.Colors.White;
            subtitleText.Visibility = Fuse.Elements.Visibility.Collapsed;
            subtitleText.Margin = float4(10f, 0f, 10f, 6f);
            subtitleText.Name = __selector3;
            subtitleText.SourceLineNumber = 42;
            subtitleText.SourceFileName = "Pages/NewsfeedPage.ux";
            subtitleText.Children.Add(temp7);
            subtitleText.Bindings.Add(temp9);
            temp7.SourceLineNumber = 43;
            temp7.SourceFileName = "Pages/NewsfeedPage.ux";
            temp7.Animators.Add(temp8);
            temp8.Value = Fuse.Elements.Visibility.Visible;
            temp3.SourceLineNumber = 42;
            temp3.SourceFileName = "Pages/NewsfeedPage.ux";
            temp10.Value = "19 julio 2018";
            temp10.FontSize = 10f;
            temp10.Color = Fuse.Drawing.Colors.White;
            temp10.Margin = float4(10f, 0f, 10f, 4f);
            temp10.SourceLineNumber = 47;
            temp10.SourceFileName = "Pages/NewsfeedPage.ux";
            temp11.SourceLineNumber = 49;
            temp11.SourceFileName = "Pages/NewsfeedPage.ux";
            temp11.Fills.Add(temp12);
            temp12.StartPoint = float2(0f, 0f);
            temp12.EndPoint = float2(0f, 1f);
            temp12.Opacity = 0.8f;
            temp12.Stops.Add(temp13);
            temp12.Stops.Add(temp14);
            temp13.Offset = 0f;
            temp13.Color = Fuse.Drawing.Colors.Transparent;
            temp14.Offset = 0.6f;
            temp14.Color = float4(0f, 0f, 0f, 1f);
            temp1.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
            temp1.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp1.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp1.SourceLineNumber = 55;
            temp1.SourceFileName = "Pages/NewsfeedPage.ux";
            temp1.Bindings.Add(temp15);
            temp4.SourceLineNumber = 55;
            temp4.SourceFileName = "Pages/NewsfeedPage.ux";
            __self.Children.Add(temp5);
            __self.Children.Add(temp11);
            __self.Children.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "Visibility";
        static global::Uno.UX.Selector __selector2 = "Url";
        static global::Uno.UX.Selector __selector3 = "subtitleText";
    }
    global::Uno.UX.Property<Fuse.Elements.Visibility> addMenuPanel_Visibility_inst;
    global::Uno.UX.Property<object> theEach_Items_inst;
    internal global::Fuse.Controls.DockPanel addMenuPanel;
    internal global::Fuse.Triggers.WhileTrue showMenu;
    internal global::Fuse.Controls.ScrollView scrollView;
    internal global::Fuse.Reactive.Each theEach;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "addMenuPanel",
        "showMenu",
        "scrollView",
        "theEach"
    };
    static NewsfeedPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public NewsfeedPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        addMenuPanel = new global::Fuse.Controls.DockPanel();
        addMenuPanel_Visibility_inst = new AppHistZone_FuseElementsElement_Visibility_Property(addMenuPanel, __selector0);
        theEach = new global::Fuse.Reactive.Each();
        theEach_Items_inst = new AppHistZone_FuseReactiveEach_Items_Property(theEach, __selector1);
        var temp = new global::Fuse.Reactive.Data("newsList");
        var temp1 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp2 = new global::Fuse.Controls.Panel();
        var temp3 = new global::Fuse.Controls.DockPanel();
        var temp4 = new global::Fuse.Controls.Panel();
        var temp5 = new global::Fuse.Controls.DockPanel();
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Fuse.Controls.Panel();
        var temp8 = new global::Fuse.Controls.Panel();
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::Fuse.Controls.Rectangle();
        var temp11 = new global::Fuse.Gestures.Clicked();
        var temp12 = new global::Fuse.Triggers.Actions.Toggle();
        showMenu = new global::Fuse.Triggers.WhileTrue();
        var temp13 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(addMenuPanel_Visibility_inst);
        var temp14 = new global::Fuse.Controls.DockPanel();
        var temp15 = new global::Fuse.Controls.DockPanel();
        var temp16 = new global::Fuse.Controls.Image();
        var temp17 = new global::Fuse.Controls.Image();
        var temp18 = new global::Fuse.Controls.Image();
        var temp19 = new global::Fuse.Gestures.Clicked();
        var temp20 = new global::Fuse.Triggers.Actions.Toggle();
        scrollView = new global::Fuse.Controls.ScrollView();
        var temp21 = new global::Fuse.Controls.StackPanel();
        var temp22 = new Template(this, this);
        var temp23 = new global::Fuse.Reactive.DataBinding(theEach_Items_inst, temp, Fuse.Reactive.BindingMode.Default);
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        global::Fuse.Controls.Navigator.SetReuse(this, Fuse.Controls.ReuseType.Any);
        temp1.LineNumber = 4;
        temp1.FileName = "Pages/NewsfeedPage.ux";
        temp1.SourceLineNumber = 4;
        temp1.SourceFileName = "Pages/NewsfeedPage.ux";
        temp1.File = new global::Uno.UX.BundleFileSource(import("../../Pages/NewsfeedPage.js"));
        temp2.SourceLineNumber = 7;
        temp2.SourceFileName = "Pages/NewsfeedPage.ux";
        temp2.Children.Add(temp3);
        temp3.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp3.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp3.SourceLineNumber = 13;
        temp3.SourceFileName = "Pages/NewsfeedPage.ux";
        temp3.Children.Add(addMenuPanel);
        addMenuPanel.Visibility = Fuse.Elements.Visibility.Collapsed;
        addMenuPanel.Name = __selector2;
        addMenuPanel.SourceLineNumber = 14;
        addMenuPanel.SourceFileName = "Pages/NewsfeedPage.ux";
        addMenuPanel.Children.Add(temp4);
        temp4.SourceLineNumber = 1;
        temp4.SourceFileName = "Elements/Menu.ux";
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp10);
        temp4.Children.Add(showMenu);
        temp5.SourceLineNumber = 4;
        temp5.SourceFileName = "Elements/Menu.ux";
        temp5.Children.Add(temp6);
        temp6.Color = float4(0.1098039f, 0.1411765f, 0.1921569f, 1f);
        temp6.Width = new Uno.UX.Size(75f, Uno.UX.Unit.Percent);
        temp6.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp6.SourceLineNumber = 5;
        temp6.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp6, Fuse.Layouts.Dock.Left);
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp6.Children.Add(temp9);
        temp7.Color = Fuse.Drawing.Colors.Green;
        temp7.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp7.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        temp7.SourceLineNumber = 7;
        temp7.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Top);
        temp8.Color = Fuse.Drawing.Colors.Yellow;
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.SourceLineNumber = 9;
        temp8.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Fill);
        temp9.Color = Fuse.Drawing.Colors.Blue;
        temp9.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp9.Height = new Uno.UX.Size(11f, Uno.UX.Unit.Percent);
        temp9.SourceLineNumber = 11;
        temp9.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp9, Fuse.Layouts.Dock.Bottom);
        temp10.Color = Fuse.Drawing.Colors.Black;
        temp10.Opacity = 0.6f;
        temp10.Layer = Fuse.Layer.Background;
        temp10.SourceLineNumber = 17;
        temp10.SourceFileName = "Elements/Menu.ux";
        temp10.Children.Add(temp11);
        temp11.SourceLineNumber = 18;
        temp11.SourceFileName = "Elements/Menu.ux";
        temp11.Actions.Add(temp12);
        temp12.SourceLineNumber = 19;
        temp12.SourceFileName = "Elements/Menu.ux";
        temp12.Target = showMenu;
        showMenu.Name = __selector3;
        showMenu.SourceLineNumber = 25;
        showMenu.SourceFileName = "Elements/Menu.ux";
        showMenu.Animators.Add(temp13);
        temp13.Value = Fuse.Elements.Visibility.Visible;
        temp14.Color = float4(0.654902f, 0.6431373f, 0.6352941f, 1f);
        temp14.SourceLineNumber = 22;
        temp14.SourceFileName = "Pages/NewsfeedPage.ux";
        temp14.Children.Add(temp15);
        temp14.Children.Add(scrollView);
        temp15.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp15.Height = new Uno.UX.Size(15f, Uno.UX.Unit.Percent);
        temp15.SourceLineNumber = 23;
        temp15.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp15, Fuse.Layouts.Dock.Top);
        temp15.Children.Add(temp16);
        temp15.Children.Add(temp17);
        temp15.Children.Add(temp18);
        temp16.Height = new Uno.UX.Size(31f, Uno.UX.Unit.Percent);
        temp16.Margin = float4(15f, 15f, 15f, 15f);
        temp16.Layer = Fuse.Layer.Background;
        temp16.SourceLineNumber = 24;
        temp16.SourceFileName = "Pages/NewsfeedPage.ux";
        temp16.File = new global::Uno.UX.BundleFileSource(import("../../Assets/NewsfeedTitle.png"));
        temp17.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp17.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp17.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp17.Layer = Fuse.Layer.Background;
        temp17.SourceLineNumber = 25;
        temp17.SourceFileName = "Pages/NewsfeedPage.ux";
        temp17.File = new global::Uno.UX.BundleFileSource(import("../../Assets/NewsfeedBackground.png"));
        temp18.Height = new Uno.UX.Size(20f, Uno.UX.Unit.Percent);
        temp18.Margin = float4(15f, 15f, 15f, 15f);
        temp18.SourceLineNumber = 27;
        temp18.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp18, Fuse.Layouts.Dock.Left);
        temp18.File = new global::Uno.UX.BundleFileSource(import("../../Assets/IconMenu.png"));
        temp18.Children.Add(temp19);
        temp19.SourceLineNumber = 28;
        temp19.SourceFileName = "Pages/NewsfeedPage.ux";
        temp19.Actions.Add(temp20);
        temp20.SourceLineNumber = 29;
        temp20.SourceFileName = "Pages/NewsfeedPage.ux";
        temp20.Target = showMenu;
        scrollView.Name = __selector4;
        scrollView.SourceLineNumber = 35;
        scrollView.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(scrollView, Fuse.Layouts.Dock.Fill);
        scrollView.Children.Add(temp21);
        temp21.SourceLineNumber = 36;
        temp21.SourceFileName = "Pages/NewsfeedPage.ux";
        temp21.Children.Add(theEach);
        theEach.Name = __selector5;
        theEach.SourceLineNumber = 37;
        theEach.SourceFileName = "Pages/NewsfeedPage.ux";
        theEach.Templates.Add(temp22);
        theEach.Bindings.Add(temp23);
        temp.SourceLineNumber = 37;
        temp.SourceFileName = "Pages/NewsfeedPage.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(addMenuPanel);
        __g_nametable.Objects.Add(showMenu);
        __g_nametable.Objects.Add(scrollView);
        __g_nametable.Objects.Add(theEach);
        this.Children.Add(temp1);
        this.Children.Add(temp2);
        this.Children.Add(temp14);
    }
    static global::Uno.UX.Selector __selector0 = "Visibility";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "addMenuPanel";
    static global::Uno.UX.Selector __selector3 = "showMenu";
    static global::Uno.UX.Selector __selector4 = "scrollView";
    static global::Uno.UX.Selector __selector5 = "theEach";
}
