[Uno.Compiler.UxGenerated]
public partial class Sidebar: Fuse.Controls.Panel
{
    internal global::Fuse.Controls.Circle userImage;
    static Sidebar()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Sidebar()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Navigation.WhileInactive();
        var temp1 = new global::Fuse.Animations.Move();
        var temp2 = new global::Fuse.Controls.DockPanel();
        var temp3 = new global::Fuse.Controls.DockPanel();
        var temp4 = new global::Fuse.Controls.Panel();
        var temp5 = new global::Fuse.Controls.Image();
        var temp6 = new global::Fuse.Controls.StackPanel();
        userImage = new global::Fuse.Controls.Circle();
        var temp7 = new global::Fuse.Controls.Panel();
        var temp8 = new global::Fuse.Controls.Panel();
        this.SourceLineNumber = 1;
        this.SourceFileName = "Elements/Menu.ux";
        temp.Threshold = 0.4f;
        temp.SourceLineNumber = 3;
        temp.SourceFileName = "Elements/Menu.ux";
        temp.Animators.Add(temp1);
        temp1.X = -180f;
        temp1.Duration = 0.2;
        temp1.Delay = 0.3;
        temp1.Target = userImage;
        temp1.Easing = Fuse.Animations.Easing.CircularIn;
        temp2.SourceLineNumber = 12;
        temp2.SourceFileName = "Elements/Menu.ux";
        temp2.Children.Add(temp3);
        temp3.Color = float4(0.1098039f, 0.1411765f, 0.1921569f, 1f);
        temp3.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp3.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp3.SourceLineNumber = 13;
        temp3.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp3, Fuse.Layouts.Dock.Left);
        temp3.Children.Add(temp4);
        temp3.Children.Add(temp7);
        temp3.Children.Add(temp8);
        temp4.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp4.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        temp4.SourceLineNumber = 15;
        temp4.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp4, Fuse.Layouts.Dock.Top);
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp6);
        temp5.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp5.Width = new Uno.UX.Size(9f, Uno.UX.Unit.Percent);
        temp5.Alignment = Fuse.Elements.Alignment.TopLeft;
        temp5.Margin = float4(15f, 15f, 15f, 15f);
        temp5.SourceLineNumber = 16;
        temp5.SourceFileName = "Elements/Menu.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/IconBack.png"));
        temp6.SourceLineNumber = 18;
        temp6.SourceFileName = "Elements/Menu.ux";
        temp6.Children.Add(userImage);
        userImage.Color = Fuse.Drawing.Colors.White;
        userImage.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        userImage.Height = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        userImage.Margin = float4(0f, 30f, 0f, 0f);
        userImage.Name = __selector0;
        userImage.SourceLineNumber = 19;
        userImage.SourceFileName = "Elements/Menu.ux";
        temp7.Color = Fuse.Drawing.Colors.Yellow;
        temp7.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp7.SourceLineNumber = 25;
        temp7.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Fill);
        temp8.Color = Fuse.Drawing.Colors.Blue;
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Height = new Uno.UX.Size(11f, Uno.UX.Unit.Percent);
        temp8.SourceLineNumber = 27;
        temp8.SourceFileName = "Elements/Menu.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Bottom);
        this.Children.Add(temp);
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "userImage";
}
