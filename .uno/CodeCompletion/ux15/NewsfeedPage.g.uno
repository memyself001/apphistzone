[Uno.Compiler.UxGenerated]
public partial class NewsfeedPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly NewsfeedPage __parent;
        [Uno.WeakReference] internal readonly NewsfeedPage __parentInstance;
        public Template(NewsfeedPage parent, NewsfeedPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<Fuse.Elements.Visibility> subtitleText_Visibility_inst;
        global::Uno.UX.Property<string> subtitleText_Value_inst;
        global::Uno.UX.Property<string> temp1_Url_inst;
        internal global::Fuse.Controls.Text subtitleText;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("title");
            subtitleText = new global::Fuse.Controls.Text();
            subtitleText_Visibility_inst = new AppHistZone_FuseElementsElement_Visibility_Property(subtitleText, __selector1);
            subtitleText_Value_inst = new AppHistZone_FuseControlsTextControl_Value_Property(subtitleText, __selector0);
            var temp3 = new global::Fuse.Reactive.Data("subtitle");
            var temp1 = new global::Fuse.Controls.Image();
            temp1_Url_inst = new AppHistZone_FuseControlsImage_Url_Property(temp1, __selector2);
            var temp4 = new global::Fuse.Reactive.Data("backgroundImg_thumbnail");
            var temp5 = new global::Fuse.Controls.StackPanel();
            var temp6 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
            var temp7 = new global::Fuse.Triggers.WhileContainsText();
            var temp8 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(subtitleText_Visibility_inst);
            var temp9 = new global::Fuse.Reactive.DataBinding(subtitleText_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
            var temp10 = new global::Fuse.Controls.Text();
            var temp11 = new global::Fuse.Controls.Rectangle();
            var temp12 = new global::Fuse.Drawing.LinearGradient();
            var temp13 = new global::Fuse.Drawing.GradientStop();
            var temp14 = new global::Fuse.Drawing.GradientStop();
            var temp15 = new global::Fuse.Reactive.DataBinding(temp1_Url_inst, temp4, Fuse.Reactive.BindingMode.Default);
            __self.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            __self.Height = new Uno.UX.Size(145f, Uno.UX.Unit.Unspecified);
            __self.Margin = float4(0f, 0f, 0f, 3f);
            __self.SourceLineNumber = 43;
            __self.SourceFileName = "Pages/NewsfeedPage.ux";
            temp5.Alignment = Fuse.Elements.Alignment.Bottom;
            temp5.SourceLineNumber = 44;
            temp5.SourceFileName = "Pages/NewsfeedPage.ux";
            temp5.Children.Add(temp);
            temp5.Children.Add(subtitleText);
            temp5.Children.Add(temp10);
            temp.FontSize = 19f;
            temp.Color = Fuse.Drawing.Colors.White;
            temp.Margin = float4(10f, 0f, 10f, 2f);
            temp.SourceLineNumber = 45;
            temp.SourceFileName = "Pages/NewsfeedPage.ux";
            temp.Bindings.Add(temp6);
            temp2.SourceLineNumber = 45;
            temp2.SourceFileName = "Pages/NewsfeedPage.ux";
            subtitleText.FontSize = 15f;
            subtitleText.Color = Fuse.Drawing.Colors.White;
            subtitleText.Visibility = Fuse.Elements.Visibility.Collapsed;
            subtitleText.Margin = float4(10f, 0f, 10f, 6f);
            subtitleText.Name = __selector3;
            subtitleText.SourceLineNumber = 46;
            subtitleText.SourceFileName = "Pages/NewsfeedPage.ux";
            subtitleText.Children.Add(temp7);
            subtitleText.Bindings.Add(temp9);
            temp7.SourceLineNumber = 47;
            temp7.SourceFileName = "Pages/NewsfeedPage.ux";
            temp7.Animators.Add(temp8);
            temp8.Value = Fuse.Elements.Visibility.Visible;
            temp3.SourceLineNumber = 46;
            temp3.SourceFileName = "Pages/NewsfeedPage.ux";
            temp10.Value = "19 julio 2018";
            temp10.FontSize = 10f;
            temp10.Color = Fuse.Drawing.Colors.White;
            temp10.Margin = float4(10f, 0f, 10f, 4f);
            temp10.SourceLineNumber = 51;
            temp10.SourceFileName = "Pages/NewsfeedPage.ux";
            temp11.SourceLineNumber = 53;
            temp11.SourceFileName = "Pages/NewsfeedPage.ux";
            temp11.Fills.Add(temp12);
            temp12.StartPoint = float2(0f, 0f);
            temp12.EndPoint = float2(0f, 1f);
            temp12.Opacity = 0.8f;
            temp12.Stops.Add(temp13);
            temp12.Stops.Add(temp14);
            temp13.Offset = 0f;
            temp13.Color = Fuse.Drawing.Colors.Transparent;
            temp14.Offset = 0.6f;
            temp14.Color = float4(0f, 0f, 0f, 1f);
            temp1.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
            temp1.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp1.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp1.SourceLineNumber = 59;
            temp1.SourceFileName = "Pages/NewsfeedPage.ux";
            temp1.Bindings.Add(temp15);
            temp4.SourceLineNumber = 59;
            temp4.SourceFileName = "Pages/NewsfeedPage.ux";
            __self.Children.Add(temp5);
            __self.Children.Add(temp11);
            __self.Children.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "Visibility";
        static global::Uno.UX.Selector __selector2 = "Url";
        static global::Uno.UX.Selector __selector3 = "subtitleText";
    }
    global::Uno.UX.Property<float> mainAppTranslation_X_inst;
    global::Uno.UX.Property<object> theEach_Items_inst;
    internal global::Fuse.Controls.EdgeNavigator EdgeNavigator;
    internal global::Sidebar menu;
    internal global::Fuse.Reactive.EventBinding temp_eb0;
    internal global::Fuse.Reactive.EventBinding temp_eb1;
    internal global::Fuse.Controls.DockPanel content;
    internal global::Fuse.Translation mainAppTranslation;
    internal global::Fuse.Controls.ScrollView scrollView;
    internal global::Fuse.Reactive.Each theEach;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "EdgeNavigator",
        "menu",
        "temp_eb0",
        "temp_eb1",
        "content",
        "mainAppTranslation",
        "scrollView",
        "theEach"
    };
    static NewsfeedPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public NewsfeedPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        mainAppTranslation = new global::Fuse.Translation();
        mainAppTranslation_X_inst = new AppHistZone_FuseTranslation_X_Property(mainAppTranslation, __selector0);
        var temp = new global::Fuse.Reactive.Data("setSidebarOpen");
        var temp1 = new global::Fuse.Reactive.Data("setSidebarClosed");
        theEach = new global::Fuse.Reactive.Each();
        theEach_Items_inst = new AppHistZone_FuseReactiveEach_Items_Property(theEach, __selector1);
        var temp2 = new global::Fuse.Reactive.Data("newsList");
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        EdgeNavigator = new global::Fuse.Controls.EdgeNavigator();
        menu = new global::Sidebar();
        var temp4 = new global::Fuse.Navigation.ActivatingAnimation();
        var temp5 = new global::Fuse.Animations.Change<float>(mainAppTranslation_X_inst);
        var temp6 = new global::Fuse.Navigation.WhileActive();
        var temp7 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb0 = new global::Fuse.Reactive.EventBinding(temp);
        var temp8 = new global::Fuse.Navigation.WhileInactive();
        var temp9 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb1 = new global::Fuse.Reactive.EventBinding(temp1);
        content = new global::Fuse.Controls.DockPanel();
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Controls.Image();
        var temp12 = new global::Fuse.Controls.Image();
        var temp13 = new global::Fuse.Controls.Image();
        scrollView = new global::Fuse.Controls.ScrollView();
        var temp14 = new global::Fuse.Controls.StackPanel();
        var temp15 = new Template(this, this);
        var temp16 = new global::Fuse.Reactive.DataBinding(theEach_Items_inst, temp2, Fuse.Reactive.BindingMode.Default);
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        global::Fuse.Controls.Navigator.SetReuse(this, Fuse.Controls.ReuseType.Any);
        temp3.LineNumber = 4;
        temp3.FileName = "Pages/NewsfeedPage.ux";
        temp3.SourceLineNumber = 4;
        temp3.SourceFileName = "Pages/NewsfeedPage.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/NewsfeedPage.js"));
        EdgeNavigator.Name = __selector2;
        EdgeNavigator.SourceLineNumber = 6;
        EdgeNavigator.SourceFileName = "Pages/NewsfeedPage.ux";
        EdgeNavigator.Children.Add(menu);
        EdgeNavigator.Children.Add(content);
        menu.Width = new Uno.UX.Size(280f, Uno.UX.Unit.Unspecified);
        menu.Name = __selector3;
        menu.SourceLineNumber = 7;
        menu.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Navigation.EdgeNavigation.SetEdge(menu, Fuse.Navigation.NavigationEdge.Left);
        menu.Children.Add(temp4);
        menu.Children.Add(temp6);
        menu.Children.Add(temp8);
        temp4.SourceLineNumber = 8;
        temp4.SourceFileName = "Pages/NewsfeedPage.ux";
        temp4.Animators.Add(temp5);
        temp5.Value = 280f;
        temp6.SourceLineNumber = 18;
        temp6.SourceFileName = "Pages/NewsfeedPage.ux";
        temp6.Actions.Add(temp7);
        temp6.Bindings.Add(temp_eb0);
        temp7.SourceLineNumber = 19;
        temp7.SourceFileName = "Pages/NewsfeedPage.ux";
        temp7.Handler += temp_eb0.OnEvent;
        temp.SourceLineNumber = 19;
        temp.SourceFileName = "Pages/NewsfeedPage.ux";
        temp8.SourceLineNumber = 21;
        temp8.SourceFileName = "Pages/NewsfeedPage.ux";
        temp8.Actions.Add(temp9);
        temp8.Bindings.Add(temp_eb1);
        temp9.SourceLineNumber = 22;
        temp9.SourceFileName = "Pages/NewsfeedPage.ux";
        temp9.Handler += temp_eb1.OnEvent;
        temp1.SourceLineNumber = 22;
        temp1.SourceFileName = "Pages/NewsfeedPage.ux";
        content.Color = float4(0.654902f, 0.6431373f, 0.6352941f, 1f);
        content.Name = __selector4;
        content.SourceLineNumber = 27;
        content.SourceFileName = "Pages/NewsfeedPage.ux";
        content.Children.Add(mainAppTranslation);
        content.Children.Add(temp10);
        content.Children.Add(scrollView);
        mainAppTranslation.Name = __selector5;
        mainAppTranslation.SourceLineNumber = 29;
        mainAppTranslation.SourceFileName = "Pages/NewsfeedPage.ux";
        temp10.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp10.Height = new Uno.UX.Size(15f, Uno.UX.Unit.Percent);
        temp10.SourceLineNumber = 31;
        temp10.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp10, Fuse.Layouts.Dock.Top);
        temp10.Children.Add(temp11);
        temp10.Children.Add(temp12);
        temp10.Children.Add(temp13);
        temp11.Height = new Uno.UX.Size(31f, Uno.UX.Unit.Percent);
        temp11.Margin = float4(15f, 15f, 15f, 15f);
        temp11.Layer = Fuse.Layer.Background;
        temp11.SourceLineNumber = 32;
        temp11.SourceFileName = "Pages/NewsfeedPage.ux";
        temp11.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/NewsfeedTitle.png"));
        temp12.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp12.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp12.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp12.Layer = Fuse.Layer.Background;
        temp12.SourceLineNumber = 33;
        temp12.SourceFileName = "Pages/NewsfeedPage.ux";
        temp12.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/NewsfeedBackground.png"));
        temp13.Height = new Uno.UX.Size(20f, Uno.UX.Unit.Percent);
        temp13.Margin = float4(15f, 15f, 15f, 15f);
        temp13.SourceLineNumber = 35;
        temp13.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Left);
        temp13.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/IconMenu.png"));
        scrollView.Name = __selector6;
        scrollView.SourceLineNumber = 39;
        scrollView.SourceFileName = "Pages/NewsfeedPage.ux";
        global::Fuse.Controls.DockPanel.SetDock(scrollView, Fuse.Layouts.Dock.Fill);
        scrollView.Children.Add(temp14);
        temp14.SourceLineNumber = 40;
        temp14.SourceFileName = "Pages/NewsfeedPage.ux";
        temp14.Children.Add(theEach);
        theEach.Name = __selector7;
        theEach.SourceLineNumber = 41;
        theEach.SourceFileName = "Pages/NewsfeedPage.ux";
        theEach.Templates.Add(temp15);
        theEach.Bindings.Add(temp16);
        temp2.SourceLineNumber = 41;
        temp2.SourceFileName = "Pages/NewsfeedPage.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(EdgeNavigator);
        __g_nametable.Objects.Add(menu);
        __g_nametable.Objects.Add(temp_eb0);
        __g_nametable.Objects.Add(temp_eb1);
        __g_nametable.Objects.Add(content);
        __g_nametable.Objects.Add(mainAppTranslation);
        __g_nametable.Objects.Add(scrollView);
        __g_nametable.Objects.Add(theEach);
        this.Children.Add(temp3);
        this.Children.Add(EdgeNavigator);
    }
    static global::Uno.UX.Selector __selector0 = "X";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "EdgeNavigator";
    static global::Uno.UX.Selector __selector3 = "menu";
    static global::Uno.UX.Selector __selector4 = "content";
    static global::Uno.UX.Selector __selector5 = "mainAppTranslation";
    static global::Uno.UX.Selector __selector6 = "scrollView";
    static global::Uno.UX.Selector __selector7 = "theEach";
}
