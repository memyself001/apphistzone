[Uno.Compiler.UxGenerated]
public partial class SplashPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router"
    };
    static SplashPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SplashPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp1 = new global::Fuse.Controls.Image();
        var temp2 = new global::Fuse.Controls.Rectangle();
        var temp3 = new global::Fuse.Drawing.LinearGradient();
        var temp4 = new global::Fuse.Drawing.GradientStop();
        var temp5 = new global::Fuse.Drawing.GradientStop();
        var temp6 = new global::Fuse.Controls.Image();
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        global::Fuse.Controls.Navigator.SetReuse(this, Fuse.Controls.ReuseType.Any);
        temp.LineNumber = 4;
        temp.FileName = "Pages/SplashPage.ux";
        temp.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/SplashPage.js"));
        temp1.StretchMode = Fuse.Elements.StretchMode.Uniform;
        temp1.Width = new Uno.UX.Size(23f, Uno.UX.Unit.Percent);
        temp1.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/SplashLogo.png"));
        temp2.Opacity = 0.43f;
        temp2.Fills.Add(temp3);
        temp3.StartPoint = float2(0f, 0f);
        temp3.EndPoint = float2(0f, 1f);
        temp3.Stops.Add(temp4);
        temp3.Stops.Add(temp5);
        temp4.Offset = 0f;
        temp4.Color = float4(0.5254902f, 0.3882353f, 0.3294118f, 1f);
        temp5.Offset = 1f;
        temp5.Color = float4(1f, 0.3176471f, 0f, 1f);
        temp6.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp6.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp6.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp6.Layer = Fuse.Layer.Background;
        temp6.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/SplashBackground.png"));
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        this.Children.Add(temp);
        this.Children.Add(temp1);
        this.Children.Add(temp2);
        this.Children.Add(temp6);
    }
}
